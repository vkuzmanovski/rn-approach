library("igraph");

buildNetworkFromTimeSeries <- function(dsPath = "./datasets",
                                       outPath = "./output",
                                       dsPrefix = "DS_",
                                       gsPrefix = "GS_",
                                       dsSep = ",",
                                       gsSep = ",",
                                       replicates = 0,
                                       includesHeader = FALSE,
                                       includesGSHeader = FALSE,
                                       rowNames = FALSE,
                                       timeByRow = FALSE,
                                       gsType = "adj",
                                       gsCommon = TRUE,
                                       dsFuncs=NULL,
                                       gsFuncs=NULL,
                                       simsGroup=c("MI","CORR","DIST","SYM"),
                                       sims=NULL,
                                       scores=c("ID+TS","AWE","AWE+TS","CLR+TS","MRNET+TS","ARACNE+TS"),
                                       ts="spearman",
                                       saveObjects=TRUE,
                                       evalName="Default",
                                       doEval="FALSE"){

  #dsPath <- "./datasets";
  #dsPrefix <- "DS_"; 
  #gsPrefix <- "GS_";
  #dsSep <- "\t";
  #includesHeader <- FALSE;
  #timeByRow <- TRUE; #default to FALSE |||| First column is always a header for time points!!!!!!
  #gsType <- "matrix"; # types: adj, fulladj (with 1's as true, 0's as false)
  #gsCommon <- TRUE; #when one gs, do I need to consider as common for all ds
  #ts <- "spearman" or "pearson"
  
  ## Gold standard must not have any headers
  
  ### Start the script
  print(paste("Execution has started at: ",Sys.time(),sep=""));
  Rprof ( tf <- "./log/execution.resources",  memory.profiling = TRUE );
  
  ### Global settings
  availableGSTypes <- c("adj","fulladj","matrix");
  availableTS <- c("pearson","spearman");
  availableGroups <- c("MI","CORR","DIST","SYM");
  availableSimilarities <- vector("list", length(availableGroups));
  names(availableSimilarities) <- availableGroups;
  
  availableSimilarities$MI <- c("efMImm","ewMImm","efMIempirical","ewMIempirical","efMIshrink","ewMIshrink"); #,"efMIml_gc","ewMIml_gc","efMImm_gc","ewMImm_gc","efMIshrink_gc","ewMIshrink_gc","Granger"
  availableSimilarities$CORR <- c("Pearson","Spearman","Kendall");
  availableSimilarities$DIST <- c("Manhattan","Euclidean","L10Norm","DTWasym","DTWsym1","DTWsym2");
  availableSimilarities$SYM <- c("efSym","ewSym","efSymMI","ewSymMI","efAvgSymMI","ewAvgSymMI","Qual");
  
  ##Check wheather input params contains right set
  if(!is.numeric(replicates)){
    stop("Replicates must be an integer. 0 means no replicates.", call. = TRUE);
  }
  if(replicates > 0 && rowNames == TRUE){
    rowNames = FALSE;
    print("RowNames has been set to FALSE, since replicates are present.");
  }
  
  if(!all(ts %in% availableTS)){
    stop("Time shifting is not correct. Available options are: ",paste(availableTS,", ",sep=""), call. = TRUE);
  }
  if(!all(gsType %in% availableGSTypes)){
    stop("Type of the gold standard file is not correct. Available options are: ",paste(availableGSTypes,", ",sep=""), call. = TRUE);
  }
  
  if(is.null(sims)){
    if(!all(simsGroup %in% availableGroups)){
      stop("List of simsGroup is not valid. Available groups are: \"MI\", \"CORR\", \"DIST\" and/or \"SYM\".", call. = TRUE);
    }
    
    sims <- unlist(lapply(simsGroup, function(group){availableSimilarities[[group]]}), recursive=TRUE, use.names=FALSE);
    
  } else {
    if(!all(sims %in% unlist(availableSimilarities,recursive = TRUE, use.names = FALSE))){
      stop("List of similarities (sims) is not valid. Available similarities/distances are: ",paste(unlist(availableSimilarities,recursive = TRUE, use.names = FALSE),", ",sep=""), call. = TRUE);
    }
  }
  
  ### LOADING DATASETS AND GOLD STANDARDS
  
  if(dir.exists(dsPath)){
    
    #get dataset files only
    listDatasetPaths <- list.files(dsPath,pattern=paste("^",dsPrefix,"*",sep=""));
    listDatasetFiles <- listDatasetPaths[!dir.exists(paste(dsPath,listDatasetPaths,sep="/"))];
    
    if(length(listDatasetFiles) > 0){
      
      loadedDatasets <- vector("list", length(listDatasetFiles));
      names(loadedDatasets) <- listDatasetFiles;
      
      for(ds in 1:length(listDatasetFiles)){
        if(timeByRow == TRUE){
          if(rowNames == TRUE){
            loadingDataset <- read.table(paste(dsPath,listDatasetFiles[ds],sep="/"),header=includesHeader,sep=dsSep,row.names=1);  
          } else {
            loadingDataset <- read.table(paste(dsPath,listDatasetFiles[ds],sep="/"),header=includesHeader,sep=dsSep);  
          }
          
          if(replicates > 0){
            rNames <- unique(loadingDataset[,1]);
            
            if(length(rNames)*replicates != length(loadingDataset[,1])){
              stop("Replicates are not correct. Number of rows does not match with unique time points.", call. = TRUE);
            }
            
            replicatedDataset <- matrix(ncol=dim(loadingDataset)[2]);
            colnames(replicatedDataset) <- colnames(loadingDataset);
            
            if(is.na(replicatedDataset[1,1])) replicatedDataset <- replicatedDataset[-1,];
            
            for(rName in 1:length(rNames)){
              subRows <- loadingDataset[which(loadingDataset[,1] == rNames[rName]),];
              replicatedDataset <- rbind(replicatedDataset,colMeans(subRows));
            }
            row.names(replicatedDataset) <- replicatedDataset[,1];
            loadingDataset <- data.frame(replicatedDataset[,-1]);
          }
          
          if(includesHeader == FALSE){
            
            nameIdx <- as.matrix(1:length(names(loadingDataset)));
            names(loadingDataset) <- apply(nameIdx, 1, FUN = function(idx) paste("Node",idx,sep=""));
          }
          
          loadedDatasets[[listDatasetFiles[ds]]] <- loadingDataset;
        }
        else {
          if(rowNames == TRUE){
            loadedDatasets[[listDatasetFiles[ds]]] <- read.table(paste(dsPath,listDatasetFiles[ds],sep="/"),header=includesHeader,sep=dsSep,row.names=1);  
          } else {
            loadedDatasets[[listDatasetFiles[ds]]] <- read.table(paste(dsPath,listDatasetFiles[ds],sep="/"),header=includesHeader,sep=dsSep);  
            
            nameIdx <- as.matrix(1:length(row.names(loadedDatasets[[listDatasetFiles[ds]]])));
            row.names(loadedDatasets[[listDatasetFiles[ds]]]) <- apply(nameIdx, 1, FUN = function(idx) paste("Node",idx,sep=""));
          }
          
          #### FINISH FOR THE REPLICATES IF TIMEPOINTS ARE ON TOP!!!!!
        }
      }
      
    } else {
      loadedDatasets <- vector("list", 0);
    }
    
    
    ###get golden standard files only
    listGoldStandardPaths <- list.files(dsPath,pattern=paste("^",gsPrefix,"*",sep=""));
    listGoldStandardFiles <- listGoldStandardPaths[!dir.exists(paste(dsPath,listGoldStandardPaths,sep="/"))];
    
    if(length(listGoldStandardFiles) > 0){
      if(length(listGoldStandardFiles) == 1 && gsCommon == TRUE){
        
        loadedGoldStandards <- vector("list", length(loadedDatasets));
        names(loadedGoldStandards) <- listDatasetFiles;
        
        for(ds in 1:length(loadedGoldStandards)){
          loadedGoldStandards[[listDatasetFiles[ds]]] <- read.table(paste(dsPath,listGoldStandardFiles[1],sep="/"),header=includesGSHeader,sep=gsSep);
          
          if(gsType == "matrix" && timeByRow == TRUE){
            names(loadedGoldStandards[[listDatasetFiles[ds]]]) <- names(loadedDatasets[[listDatasetFiles[ds]]]);
            row.names(loadedGoldStandards[[listDatasetFiles[ds]]]) <- names(loadedDatasets[[listDatasetFiles[ds]]]);
          } else if(gsType == "matrix" && timeByRow == FALSE) {
            names(loadedGoldStandards[[listDatasetFiles[ds]]]) <- row.names(loadedDatasets[[listDatasetFiles[ds]]]);
            row.names(loadedGoldStandards[[listDatasetFiles[ds]]]) <- row.names(loadedDatasets[[listDatasetFiles[ds]]]);
          }
        }
        
      } else {
        loadedGoldStandards <- vector("list", length(listGoldStandardFiles));
        names(loadedGoldStandards) <- listGoldStandardFiles;
        
        for(ds in 1:length(listGoldStandardFiles)){
          dsName <- gsub("GS_","DS_",listGoldStandardFiles[ds]);
          loadedGoldStandards[[dsName]] <- read.table(paste(dsPath,listGoldStandardFiles[ds],sep="/"),header=includesGSHeader,sep=gsSep);
          
          if(!is.null(loadedDatasets[[dsName]])){
            if(gsType == "matrix" && timeByRow == TRUE){
              names(loadedGoldStandards[[dsName]]) <- names(loadedDatasets[[dsName]]);
              row.names(loadedGoldStandards[[dsName]]) <- names(loadedDatasets[[dsName]]);
            } else if(gsType == "matrix" && timeByRow == FALSE) {
              names(loadedGoldStandards[[dsName]]) <- row.names(loadedDatasets[[dsName]]);
              row.names(loadedGoldStandards[[dsName]]) <- row.names(loadedDatasets[[dsName]]);
            }
          }
        }
        
      }
    } else {
      loadedGoldStandards <- vector("list", 0);
    }

  }else{
    loadedDatasets <- vector("list", 0);
    loadedGoldStandards <- vector("list", 0);
  }
  
  if(length(loadedDatasets) > 0){
    ### PERFORM PREPROCESING
    
    ##Time By Row - Transpose
    if(timeByRow == TRUE){
      loadedDatasets <- lapply(loadedDatasets, FUN = function(ds){ t(ds) });
    }
    
    loadedGoldStandards <- loadedGoldStandards[!sapply(loadedGoldStandards, is.null)];
    ##Create adjacency matrix
    if(length(loadedGoldStandards) > 0){
      
      if(gsType == "adj" || gsType == "fulladj"){
        for(gs in 1:length(loadedGoldStandards)){
          gsEdgeList <- as.matrix(loadedGoldStandards[[gs]]);
          gsName <- names(loadedGoldStandards[gs]);
          
          if(gsCommon == TRUE){
            listNodeNames <- row.names(loadedDatasets[[1]]);
          } else {
            listNodeNames <- row.names(loadedDatasets[[gsName]]);
          }
          
          tmpGS <- matrix(0,length(listNodeNames),length(listNodeNames));
          colnames(tmpGS) <- listNodeNames;
          rownames(tmpGS) <- listNodeNames;
          
          tmpGS[gsEdgeList[which(gsEdgeList[,3] == 1),1:2]] <- 1;
          #tmpGS[gsEdgeList[,1:2]] <- 1;
          
          loadedGoldStandards[[gs]] <- as.data.frame(tmpGS);
        }
      }
    }
    
    print("######## Loading files completed!");
    print(paste("Loaded datasets - object size:",format(object.size(loadedDatasets),units = "Mb"),sep=" "));
    print(paste("Loaded gold standards - object size:",format(object.size(loadedGoldStandards),units = "Mb"),sep=" "));
    print("---------------------------------");
    
    #stop("asdasdasdasd", call. = TRUE);
    
    ### PROCESSING
    library("parallel");
    source("symbolvector.R");
    source("TS_ALGO.R");
    
    no_cores <- max(as.numeric(detectCores()/3),1);
    datasetsPredictions <- vector("list",length(loadedDatasets));
    names(datasetsPredictions) <- names(loadedDatasets);
    print(paste("Resources to be used:",no_cores,"core(s)",sep=" "));
    
    for(ds in 1:length(loadedDatasets)){
      print(paste("Processing the next dataset: ",names(loadedDatasets[ds]),sep=""));
      actualDataset <- loadedDatasets[[ds]];
      actualDatasetNNodes <- nrow(actualDataset) + 1;
      actualDatasetNObservations <- ncol(actualDataset);
      actualDatasetName <- names(loadedDatasets[ds]);
      actualDatasetSymbolicPatterns <- min(as.numeric(actualDatasetNObservations/2),4);
      
      if(length(grep("Sym",sims)) > 0){
        actualDatasetPatterns <- symbolvector(actualDataset,actualDatasetNObservations,actualDatasetSymbolicPatterns);
      } else {
        actualDatasetPatterns <- NULL;
      }
      
      print("Calculating time-shifting over original dataset...")
      if(length(grep("+TS",scores)) > 0){
        actualDatasetTSPositiveLinks <- TS_ALGO(actualDataset,actualDatasetNObservations,actualDatasetNNodes,ts,no_cores);
      } else {
        actualDatasetTSPositiveLinks <- NULL;
      }
      
      
      
      processingCluster <- makeCluster(no_cores, outfile = "./log/global_prediction.log");
      clusterEvalQ(processingCluster, library(infotheo));
      clusterEvalQ(processingCluster, library(igraph));
      clusterEvalQ(processingCluster, source("rnR_Framework.R"));
      
      print("Running first step...");
      similarityMatrices <- parSapply(processingCluster, sims, simplify = FALSE, USE.NAMES = TRUE,
                function(sim, actualDataset, actualDatasetNNodes, actualDatasetNObservations, actualDatasetName, actualDatasetSymbolicPatterns, patterns, numCores){
                  
                  ##Perform similarity/distance step
                  firstStepMatrix <- switch(sim,
                         efMImm = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="mi.mm", discretization = TRUE, discretizator = "equalfreq"),
                         ewMImm = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="mi.mm", discretization = TRUE, discretizator = "equalwidth"),
                         efMIempirical = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="mi.empirical", discretization = TRUE, discretizator = "equalfreq"),
                         ewMIempirical = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="mi.empirical", discretization = TRUE, discretizator = "equalwidth"),
                         efMIshrink = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="mi.shrink", discretization = TRUE, discretizator = "equalfreq"),
                         ewMIshrink = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="mi.shrink", discretization = TRUE, discretizator = "equalwidth"),
                         Pearson = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="pearson"),
                         Spearman = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="spearman"),
                         Kendall = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="kendall"),
                         Manhattan = getSimilarityMatrix_DISTANCES(actualDataset,actualDatasetNNodes,norms=1),
                         Euclidean = getSimilarityMatrix_DISTANCES(actualDataset,actualDatasetNNodes,norms=2),
                         L10Norm = getSimilarityMatrix_DISTANCES(actualDataset,actualDatasetNNodes,norms=10),
                         DTWasym = getSimilarityMatrix_DTW(actualDataset,steppatern=asymmetric),
                         DTWsym1 = getSimilarityMatrix_DTW(actualDataset,steppatern=symmetric1),
                         DTWsym2 = getSimilarityMatrix_DTW(actualDataset,steppatern=symmetric2),
                         efSym = getSimilarityMatrix_SYMBOLIC(actualDataset,actualDatasetNNodes,actualDatasetNObservations, simmethod="sym", npatterns=actualDatasetSymbolicPatterns, discretizator = "equalfreq", patterns=patterns, numCores),
                         ewSym = getSimilarityMatrix_SYMBOLIC(actualDataset,actualDatasetNNodes,actualDatasetNObservations, simmethod="sym", npatterns=actualDatasetSymbolicPatterns, discretizator = "equalwidth", patterns=patterns, numCores),
                         efSymMI = getSimilarityMatrix_SYMBOLIC(actualDataset,actualDatasetNNodes,actualDatasetNObservations,  simmethod="sym.mi", npatterns=actualDatasetSymbolicPatterns, discretizator = "equalfreq", patterns=patterns, numCores),
                         ewSymMI = getSimilarityMatrix_SYMBOLIC(actualDataset,actualDatasetNNodes,actualDatasetNObservations,  simmethod="sym.mi", npatterns=actualDatasetSymbolicPatterns, discretizator = "equalwidth", patterns=patterns, numCores),
                         efAvgSymMI = getSimilarityMatrix_SYMBOLIC(actualDataset,actualDatasetNNodes,actualDatasetNObservations,  simmethod="avg.sym.mi", npatterns=actualDatasetSymbolicPatterns, discretizator = "equalfreq", patterns=patterns, numCores),
                         ewAvgSymMI = getSimilarityMatrix_SYMBOLIC(actualDataset,actualDatasetNNodes,actualDatasetNObservations,  simmethod="avg.sym.mi", npatterns=actualDatasetSymbolicPatterns, discretizator = "equalwidth", patterns=patterns, numCores),
                         Qual = getSimilarityMatrix_QUAL(actualDataset,actualDatasetNNodes,actualDatasetNObservations)
                  #       efMIml_gc = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="coarse.grained", subestimators="ML",discretizator = "equalfreq"),
                  #       ewMIml_gc = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="coarse.grained", subestimators="ML",discretizator = "equalwidth"),
                  #       efMImm_gc = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="coarse.grained", subestimators="mm",discretizator = "equalfreq"),
                  #       ewMImm_gc = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="coarse.grained", subestimators="mm",discretizator = "equalwidth"),
                  #       efMIshrink_gc = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="coarse.grained", subestimators="shrink",discretizator = "equalfreq"),
                  #       ewMIshrink_gc = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="coarse.grained", subestimators="shrink",discretizator = "equalwidth"),
                  #       Granger = getSimilarityMatrix_MI(actualDataset,actualDatasetNNodes,estimators="granger")
                         )
                  
                  row.names(firstStepMatrix) <- row.names(actualDataset);
                  firstStepMatrix
                  #getNormalizedMatrix(firstStepMatrix,normalization="minimax");
                  
                },actualDataset, actualDatasetNNodes, actualDatasetNObservations, actualDatasetName, actualDatasetSymbolicPatterns, actualDatasetPatterns, 2); #max(as.numeric((detectCores()-no_cores)/6)-1,1)
      
      
      print("Running second step...");
      scorringMatrices <- parSapply(processingCluster, similarityMatrices, simplify = FALSE, USE.NAMES = TRUE,
                           function(m, scores, actualDatasetTSPositiveLinks){
                             scoreMatrices <- vector("list",length(scores));
                             names(scoreMatrices) <- scores;
                             
                             ##Perform scorring step
                             for(sc in 1:length(scores)){
                               tScorer <- gsub("+TS","",scores[sc],fixed = TRUE);
                               tTS <- length(grep("+TS",scores[sc],fixed = TRUE));
                               
                               if(scores[sc] == "ID+TS")
                               {
                                 tMat <- as.matrix(m);
                                 tMat[is.nan(tMat)] <- 0;
                                 
                                 ### Perform time shifting
                                 if(tTS > 0){
                                   tMat[which(actualDatasetTSPositiveLinks == 0)] <- 0;
                                 }
                                 
                                 scoreMatrices[[sc]] <- as.matrix(tMat);
                               }
                               else
                               {
                                 tMat <- getScorredMatrix(m,scorrer=tScorer);
                                 tMat[is.nan(tMat)] <- 0;
                                 
                                 ### Perform time shifting
                                 if(tTS > 0){
                                   tMat[which(actualDatasetTSPositiveLinks == 0)] <- 0;
                                 }
                                 
                                 scoreMatrices[[sc]] <- as.matrix(tMat);
                               }
                               
                               scoreMatrices[[sc]] <- getNormalizedMatrix(scoreMatrices[[sc]],normalization="minimax");
                             }
                             
                             scoreMatrices
                             
                           },scores, actualDatasetTSPositiveLinks);
      
      resultMatrices <- unlist(scorringMatrices,recursive = FALSE);
      resultAdjMatrices <- parSapply(processingCluster,resultMatrices, simplify = FALSE, USE.NAMES = TRUE, 
                                     function(m){
                                       graph.adjacency(m, mode="directed", weighted=TRUE);
                                     });
      
      stopCluster(processingCluster);
      
      datasetsPredictions[[ds]] <- vector("list", length = 2);
      names(datasetsPredictions[[ds]]) <- c("mPred","adjPred");
      
      datasetsPredictions[[ds]][["mPred"]] <- resultMatrices;
      datasetsPredictions[[ds]][["adjPred"]] <- resultAdjMatrices;
      
      ### Write to R data file
      predictions <- datasetsPredictions[ds];
      if(saveObjects == TRUE && !dir.exists(paste(outPath,"/objects",sep=""))) dir.create(paste(outPath,"/objects",sep=""));
      if(saveObjects == TRUE) save("predictions",file=paste(outPath,"/objects/",actualDatasetName,".RData",sep=""));
      
      print(paste("Complete processing of the dataset: ",actualDatasetName,sep=""));
    }
    
   
    
    
    ### Evaluations
    if(doEval == "TRUE" && length(loadedGoldStandards) > 0 && length(datasetsPredictions) > 0){
      datasetNames <- names(loadedGoldStandards);
      
      if(!dir.exists(paste(outPath,"/plot",sep=""))) dir.create(paste(outPath,"/plot",sep=""));
      
      source("util.R")
      for(ds in 1:length(datasetNames)){
        actualDataset <- datasetsPredictions[[ds]];
        actualGoldStandard <- loadedGoldStandards[[ds]];
        perfPerScorer <- vector("list");
        
        for(dss in 1:length(actualDataset[["mPred"]])){
          pdf(paste(outPath,"/plot/","[",datasetNames[ds],"]-",names(actualDataset[["mPred"]][dss]),"_plot.pdf",sep=""));
          ads <- actualDataset[["mPred"]][[dss]];
          perfPerScorer[[names(actualDataset[["mPred"]][dss])]] <- binary_eval(as.vector(as.matrix(ads)),as.vector(as.matrix(actualGoldStandard)),"f",drop=FALSE);
          dev.off();
        }
        datasetsPredictions[[ds]][["performance"]] <- perfPerScorer;
        # print(actualDataset$mPred[[1]][,1])
        # print(as.vector(actualDataset$mPred[[1]])[1:22])
        # print(actualGoldStandard[,1])
        # print(as.vector(as.matrix(actualGoldStandard))[1:22])
        
        # processingCluster <- makeCluster(no_cores, outfile = "./log/global_evaluation.log");
        # clusterEvalQ(processingCluster, library(ROCR));
        # clusterEvalQ(processingCluster, library(minet));
        # #clusterEvalQ(processingCluster, library(qpgraph));
        # clusterEvalQ(processingCluster, source("util.R"));
        # 
        # performace <- parSapply(processingCluster,actualDataset[["mPred"]], simplify = FALSE, USE.NAMES = TRUE,
        #                                function(ds, actualGoldStandard){
        #                                  binary_eval(as.vector(as.matrix(ds)),as.vector(as.matrix(actualGoldStandard)))
        #                                  
        #                                  # perf <- vector("list",2);
        #                                  # names(perf) <- c("prediction","scalars");
        #                                  # 
        #                                  # perf$prediction <- prediction(as.vector(as.matrix(ds)),as.vector(as.matrix(actualGoldStandard)));
        #                                  # 
        #                                  # scalars <- vector("list",7); #AUC, prbe, rmse, sar, f, acc
        #                                  # names(scalars) <- c("ACC","maxACC","RMSE", "F", "AUC", "PRBE", "SAR");
        #                                  # 
        #                                  # p <- performance(perf$prediction,measure="acc");
        #                                  # scalars$ACC <- c(p@y.values[[1]][min(which(p@x.values[[1]] >= 0.5))],min(which(p@x.values[[1]] >= 0.5)));
        #                                  # scalars$maxACC <- c(max(p@y.values[[1]]),p@x.values[[1]][which(p@y.values[[1]] == max(p@y.values[[1]]))]);
        #                                  # 
        #                                  # p <- performance(perf$prediction,measure="rmse");
        #                                  # scalars$RMSE <- round(unlist(p@y.values),2);
        #                                  # 
        #                                  # # p <- performance(perf$prediction,measure="sar");
        #                                  # # scalars$SAR <- p;#round(unlist(p@y.values),2);
        #                                  # 
        #                                  # # p <- performance(perf$prediction,measure="f");
        #                                  # # scalars$F <- p;
        #                                  # #
        #                                  # p <- performance(perf$prediction,measure="auc",fpr.stop=1.0);
        #                                  # scalars$AUC <- p;#round(unlist(p@y.values),2);
        #                                  # #
        #                                  # p <- performance(perf$prediction,measure="prbe");
        #                                  # scalars$PRBE <- p;
        #                                  # 
        #                                  # perf$scalars <- scalars;
        #                                  # 
        #                                  # perf
        #                                }, actualGoldStandard);
        # 
        # stopCluster(processingCluster);
        # 
        # print(performace);

      }


    }
    
    if(doEval == "TRUE"){
        ### Writing results into files...
        
        print("Storing results...")
        
        
        gPerformance <- matrix(ncol=10);
        
        for(ds in 1:length(datasetsPredictions)){
        
        actualDataset <- datasetsPredictions[[ds]];
        actualDatasetName <- names(datasetsPredictions[ds]);
        
        files <- unlist(actualDataset,recursive = FALSE);
        
        for(f in 1:length(files)){
            filename <- paste(outPath,"/","[",actualDatasetName,"]-",names(files[f]),".csv",sep="");
            
            
            if(length(grep("adjPred",filename, fixed=TRUE)) > 0){
            write.graph(files[[f]], filename, format = "ncol");
            } else if(length(grep("mPred",filename, fixed=TRUE)) > 0){
            write.csv(files[[f]],file = filename);
            } else {
            
            if(length(colnames(gPerformance)) == 0){
                colnames(gPerformance) <- c(c("Dataset","Method"),as.vector(names(files[[f]])));  
            }
            
            for(dset in 1:length(files[f])){
                gPerformance <- rbind(gPerformance,c(actualDatasetName,gsub("performance.","",names(files[f][dset])),round(as.vector(files[f][[dset]]),4)))
            }
            }
        }
        }
        
        gPerformance <- gPerformance[-1,];
        write.csv(gPerformance,file = paste(outPath,"/",evalName,"-Performance-",gsub(":","-",Sys.time()),".csv",sep=""));
    } else {
        print("Performed without evaluation!");
    }
    
  } else {
    print("No datasets found on the path specified");
  }
  
  Rprof ( NULL ) ; print ( summaryRprof ( tf )  );
  print(paste("Execution has finished at: ",Sys.time(),sep=""));
}
