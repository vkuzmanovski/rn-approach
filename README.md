# Network inference - Relevance Network Approach

The complete package consists of two parts:

*	Network reconstruction with GRN variants
*	Analysis of the results of network reconstruction with GRN variants

## Network reconstruction with GRN variants]

This module of the whole package can be used for reconstruction of a network by given dataset (either time_series or steady-state data poinst).
The framework core is given in `rnR_Framework.R` and `RelNR_TS.R` or `RelNR_SS.R` for time-series or steady-state data, respectively. The framework can be utilized through two functions: `buildNetworkFromTS()` or `buildNetworkFromSS()`. Examples of function utilization is given in `_start.R`.

The package includes also datasets that has been used within the study. In order to run the same datasets (or dataset modules) in command line run 
`Rscript _start.R <DATA_TYPE> <DATA_MODULE>`. where <DATA_TYPE> can be `TS` or `SS` and <DATA_MODULE>: `hempel-ds`,`dream5-ds`,`gasch-ds`,`irma-ds` or `dream4-ds`. 

The provided script `_start.R` will also install all required packages/dependencies. 

**It is important to note that the datasets are provided as archive, so it is required to be un-archived and placed in folder "datasets"**.

## Analysis of the results of network reconstruction with GRN variants]

The scripts for analysis of the results of network reconstruction are in the folder analysis. The subfolders "steady-state" and "time-series" correspond to the analysis of the results obtained from steady-state and time-series data, respectively.

In each folder, the main source file is analysis.R, which can be started in any environment that supports the use of R environment for statistical computing. In particular, from command-line interface on Unix-like systems, one can use the command

`R --no-save < analysis.R > analysis.log`

to start the script and obtain the graphs from the article section "Results". Many graphs are being produced the scripts, in addition to intermediate ones that have been used for checking the script correctness, the ones included in the article are the ones with the prefixes "bp-assoc" and "bp-score".

To run analysis.R in the folder "steady-state", the following files are needed as input:

*	nr-ss-ds-nets.csv
*	nr-ss-methods.csv
*	nr-ss-nets.csv
*	nr-ss-ds.csv
*	nr-ss-metrics.csv
*	nr-ss-results.csv

Similarly, to run analysis.R in the folder "time-series", the following files are needed as input:

*	nr-methods.csv
*	nr-nets.csv
*	nr-ts-nets.csv
*	nr-metrics.csv
*	nr-results.csv
*	nr-ts.csv

**IMPORTANT: Unarchive the archive "analysis.tar.gz" in order to get the given analysis space**.
